<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materials".
 *
 * @property int $materials_id
 * @property int $project_id
 * @property string $item
 * @property float|null $quantity
 * @property string|null $unit
 * @property float|null $rate
 * @property float $total
 * @property int $type
 * @property string $date_created
 *
 * @property Projects $project
 */
class Materials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'item', 'total'], 'required'],
            [['project_id', 'type'], 'integer'],
            [['item'], 'string'],
            [['quantity', 'rate', 'total'], 'number'],
            [['date_created'], 'safe'],
            [['unit'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'project_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'materials_id' => 'Materials ID',
            'project_id' => 'Project ID',
            'item' => 'Item',
            'quantity' => 'Quantity',
            'unit' => 'Unit',
            'rate' => 'Rate',
            'total' => 'Total',
            'type' => 'Type',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['project_id' => 'project_id']);
    }
}
