<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_queue".
 *
 * @property int $queue_id
 * @property string|null $phone
 * @property string $message
 * @property string $status
 * @property int|null $batch_id
 * @property int $customer_id
 *
 * @property Customers $customer
 * @property SmsBatch $batch
 */
class SmsQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'customer_id'], 'required'],
            [['message'], 'string'],
            [['batch_id', 'customer_id'], 'integer'],
            [['phone', 'status'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
            [['batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsBatch::className(), 'targetAttribute' => ['batch_id' => 'batch_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'queue_id' => 'Queue ID',
            'phone' => 'Phone',
            'message' => 'Message',
            'status' => 'Status',
            'batch_id' => 'Batch ID',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[Batch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(SmsBatch::className(), ['batch_id' => 'batch_id']);
    }
}
