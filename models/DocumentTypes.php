<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document_types".
 *
 * @property int $document_type_id
 * @property string $name
 * @property int $emailed
 *
 * @property Documents[] $documents
 */
class DocumentTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['emailed'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'document_type_id' => 'Document Type ID',
            'name' => 'Name',
            'emailed' => 'Emailed',
        ];
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['document_type_id' => 'document_type_id']);
    }
}
