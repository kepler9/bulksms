<?php
namespace app\models;
use Yii;
use yii\validators\NumberValidator;
/**
 * Signup form
 */
class ExcelFileUpload extends Models {
    public $UploadFile;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UploadFile'], 'required'],
            [['UploadFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx'],
        ];
    }
    public function upload() {
        if ($this->validate()) {
            if($this->UploadFile->saveAs('uploads/' . $this->UploadFile->baseName . '.' . $this->UploadFile->extension)){
                return true;
            }
        }
        return false;
    }
    public function createCustomers($params){
        $SavedFilePath = "uploads/".$this->UploadFile->baseName . '.' . $this->UploadFile->extension;
        $InputFile = \PhpOffice\PhpSpreadsheet\IOFactory::identify($SavedFilePath);
        $Reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($InputFile);
        $objPHPExcel = $Reader->load($SavedFilePath);
        $Sheet = $objPHPExcel->getsheet(0);
        $HiggestRow = $Sheet->getHighestDataRow();
        $HiggestColumn = $Sheet->getHighestColumn();
        $Transaction = Yii::$app->db->beginTransaction();
        try{             
            for ($row = 2; $row <= $HiggestRow; $row++) {
                $RowData = $Sheet->rangeToArray('A' . $row . ':'.$HiggestColumn . $row, NULL, TRUE, FALSE);
                $RowData = $RowData[0];
                if(isset($RowData[1]) && $RowData[1]){
                    $phone = $this->formatPhone($RowData[1]);
                    $validator = new NumberValidator();
                    if($validator->validate($phone)){
                        $model = Customers::find()->where(['phone'=>$phone])->one();
                        if(!$model){
                            $model = new Customers();
                            $model->name = trim($RowData[0]);
                            $model->phone = $phone;
                            if(isset($RowData[2]) && $RowData[2]){
                                $model->email = str_replace(' ', '', $RowData[2]);
                            }
                            if(isset($RowData[3]) && $RowData[3]){
                                $model->company = trim($RowData[3]);
                            }
                            $model->list_id = $params['list_id'];
                            $model->status = 1;
                            if(!$model->save()){
                                $Transaction->rollBack();
                            }
                        }
                    }
                }
            }
            // $Transaction->commit();
            return true;
        }catch(\Exception $e){
            $Transaction->rollBack();
            throw $e;
        }
    }
}
