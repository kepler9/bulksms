<?php
return [
    'adminEmail' => 'admin@kepler9.co',
    'senderEmail' => 'mailer@kepler9.co',
    'senderName' => 'Bulk SMS',
    'company' => 'BULK SMS APP',
    'abbr' => 'BSA',
];
