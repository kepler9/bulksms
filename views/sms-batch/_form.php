<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
?>
<div class="sms-batch-form">
    <?php $form = ActiveForm::begin(); ?>
    <label>Customer List | <a href="/customer-lists/create?redirect=<?=Yii::$app->request->url?>"><i class="fa fa-plus"></i> Add</a></label>
    <?= $form->field($model,'list_id')->dropDownList(ArrayHelper::map($lists,'list_id','name'),['prompt'=>'Select'])->label(false);?>
    <?= $form->field($model, 'message')->textarea(['rows' => 3])->hint('<p style="color:#777"><span id="chars-evas">0</span> characters</p>')->label('Message<br><span style="color:#777;font-size:12px"> Merge fields [NAME]</span>') ?>
    <div class="form-group">
    	<label>When to Send</label>
	    <?= DateTimePicker::widget([
			'model' => $model,
			'attribute' => 'send_date',
			'options' => ['placeholder' => 'Enter date & time...'],
			'pluginOptions' => [
				'autoclose' => true
			]
		]); ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
	$('#smsbatch-message').keyup(function() {
		var length = $(this).val().length;
		$('#chars-evas').text(length);
	});
</script>
