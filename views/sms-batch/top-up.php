<?php
$this->title = 'Top up SMS Balance';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?> <span class="badge badge-danger">KSh <?=number_format($sms_balance,2);?></span></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
		<ul class="list-unstyled">
		  <li class="list-group-item"><b>1.</b> Using your MPesa-enabled phone, select "Pay Bill" from the M-Pesa menu</li>
		  <li class="list-group-item"><b>2.</b> Enter Africa's Talking Business Number <b>525900</b></li>
		  <li class="list-group-item"><b>3.</b> Enter your Africa's Talking Account Number. Your account number is <b>mauwest.api</b></li>
		  <li class="list-group-item"><b>4.</b> Enter the Amount of credits you want to buy</li>
		  <li class="list-group-item"><b>5.</b> Confirm that all the details are correct and press Ok</li>
		  <li class="list-group-item"><b>6.</b> Your API Balance account will also be updated or <b><a href="<?=Yii::$app->request->url?>">Refresh this page to get updated balance</a></b></li>
		</ul>
    </div>
</div>
