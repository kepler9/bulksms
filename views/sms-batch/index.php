<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Bulk SMS Schedules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Create', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="sms-batch-index">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'message:ntext',
                    ['attribute'=>'send_date','label'=>'When to Send'],
                    ['attribute'=>'list.name','label'=>'Customer List'],

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>

        </div>
    </div>
</div>