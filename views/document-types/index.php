<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Document Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Add Document Type', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                ['attribute'=>'emailed','format'=>'raw','value'=>function($model){
                    return $model->emailed ? 'Yes' : 'No';
                },'label'=>'Can be emailed to client'],

                ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
            ],
        ]); ?>
    </div>
</div>