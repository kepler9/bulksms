<?php
use yii\helpers\Html;
$this->title = 'Add a Measure Units';
$this->params['breadcrumbs'][] = ['label' => 'Measure Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="measure-units-create">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>
