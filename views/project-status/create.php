<?php
	use yii\helpers\Html;
	$this->title = 'Create Project Status';
	$this->params['breadcrumbs'][] = ['label' => 'Project Statuses', 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>