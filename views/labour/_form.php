<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use kartik\select2\Select2;
    $type = Yii::$app->request->get('type') ?? $model->type;
    if($model->date_created){
        $model->date_created = date('Y-m-d',strtotime($model->date_created));
    }
?>
<div class="estimated-labour-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php if($type == 0):?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true,'placeholder'=>'Enter title or name','data-validation'=>'required']) ?>
        <?= $form->field($model, 'total')->textInput(['maxlength' => true,'placeholder'=>'Enter total amount']) ?>
    <?php else:?>
        <?php //$form->field($model,'casual_id')->dropDownList(ArrayHelper::map(\app\models\Casuals::find()->orderBy('name asc')->all(),'casual_id','name'),['prompt'=>'Select','data-validation'=>'required','onchange'=>'getCasualRate()'])->label('Casual');?>
        <?=$form->field($model,'casual_id')->widget(Select2::classname(), 
            [
                'data' => ArrayHelper::map(\app\models\Casuals::find()->orderBy('name asc')->asArray()->all(),'casual_id','name'),
                'options' => ['placeholder' => 'Select','data-validation'=>'required','onchange'=>'getCasualRate()'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]
        )->label('Casual');?>
        <?= $form->field($model, 'rate')->textInput(['type' => 'number','placeholder'=>'Enter amount','data-validation'=>'number'])->label('Amount') ?>
        <?= $form->field($model, 'overtime')->textInput(['type' => 'number','placeholder'=>'Enter amount']) ?>
        <?= $form->field($model, 'transport')->textInput(['type' => 'number','placeholder'=>'Enter amount']) ?>
        <?= $form->field($model, 'wht')->textInput(['type' => 'number','placeholder'=>'Enter amount'])->label('WHT') ?>
        <?= $form->field($model, 'date_created')->textInput(['type'=>'date','data-validation'=>'required'])->label('Date'); ?>
    <?php endif;?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
    function getCasualRate(){
        $.get('/labour/casual?casual_id=' + $('#labour-casual_id').val(), function(data) {
            response = JSON.parse(data);
            $('#labour-rate').val(response.daily_rate);
        });
    }
</script>
