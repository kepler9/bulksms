<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $type = Yii::$app->request->get('type') ? 'Actual' : 'Estimated';
    $this->title = $type.' Labour';
    if(Yii::$app->request->get('type')){
        $total = $this->context->model->actual_labour_cost($this->context->project->project_id);
    }else{
        $total = $this->context->model->estimated_labour_cost($this->context->project->project_id);
    }
    $this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
    $this->params['breadcrumbs'][] = ['label'=>$this->context->project->name,'url'=>['/projects/view','id'=>$this->context->project->project_id]];
    $this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>['/labour/index','type'=>Yii::$app->request->get('type')]];
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Add '.$type.' Labour', ['create','type'=>Yii::$app->request->get('type')], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="btn-group btn-rounded pull-right">
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($total).'</b>',['labour/index','type'=>Yii::$app->request->get('type')]);?></div>
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['labour/index','type'=>Yii::$app->request->get('type'),'download'=>1]);?></div>
        </div>
        <span class="clearfix"></span>
        
        <div style="margin-top:20px" class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    ['attribute'=>'wht','format'=>'decimal','filter'=>false,'label'=>'WHT'],
                    ['attribute'=>'overtime','format'=>'decimal','filter'=>false],
                    ['attribute'=>'transport','format'=>'decimal','filter'=>false],
                    ['attribute'=>'rate','format'=>'decimal','filter'=>false],
                    ['attribute'=>'total','format'=>'decimal','filter'=>false],
                    ['attribute'=>'date_created','format'=>'date','label'=>'Date'],

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>
        </div>

    </div>
</div>
