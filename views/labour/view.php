<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
    \yii\web\YiiAsset::register($this);
    $this->title = $model->title;
    $type = $model->type ? 'Actual' : 'Estimated';
    $this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
    $this->params['breadcrumbs'][] = ['label'=>$model->project->name,'url'=>['/projects/view','id'=>$model->project_id]];
    $this->params['breadcrumbs'][] = ['label' => $type.' Labour', 'url' => ['index','type'=>$model->type]];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->labour_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->labour_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                ['attribute'=>'project.name','label'=>'Project'],
                ['attribute'=>'wht','format'=>'decimal','filter'=>false,'label'=>'WHT'],
                ['attribute'=>'overtime','format'=>'decimal','filter'=>false],
                ['attribute'=>'transport','format'=>'decimal','filter'=>false],
                ['attribute'=>'rate','format'=>'decimal','filter'=>false],
                ['attribute'=>'total','format'=>'decimal','filter'=>false],
                ['attribute'=>'date_created','format'=>'date','label'=>'Date'],
            ],
        ]) ?>
    </div>
</div>
