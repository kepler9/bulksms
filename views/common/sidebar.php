<?php
    $route = Yii::$app->controller->id;
    $action = Yii::$app->controller->action->id;
?>
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <ul class="side-menu metismenu">
                <li class="level-1-menu">
                    <a class="<?= ($route == 'welcome' && $action == 'index') ? 'active' : ''; ?>" href="/"><i class="sidebar-item-icon fa fa-home"></i>
                        <span class="nav-label">Home</span>
                    </a>
                </li>
                <li class="level-1-menu">
                    <a href="javascript:;"><i class="sidebar-item-icon fa fa-user"></i>
                        <span class="nav-label">Customers</span><i class="fa fa-angle-left arrow"></i>
                    </a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="/customers/create">Create</a>
                        </li>
                        <li>
                            <a href="/customers/index">Customers</a>
                        </li>
                        <li>
                            <a href="/customer-lists/index">Customer Lists</a>
                        </li>
                    </ul>
                </li>
                <li class="level-1-menu">
                    <a class="<?= ($route == 'sites' && $action == 'index') ? 'active' : ''; ?>" href="/sites/index"><i class="sidebar-item-icon fa fa-map"></i>
                        <span class="nav-label">Site Visits</span>
                    </a>
                </li>
                <li class="level-1-menu">
                    <a href="javascript:;"><i class="sidebar-item-icon fa fa-briefcase"></i>
                        <span class="nav-label">Project Management</span><i class="fa fa-angle-left arrow"></i>
                    </a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="/projects/create">New Project</a>
                        </li>
                        <li>
                            <a href="/projects/index">Projects</a>
                        </li>
                    </ul>
                </li>
                <li class="level-1-menu">
                    <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope-o"></i>
                        <span class="nav-label">Bulk SMS</span><i class="fa fa-angle-left arrow"></i>
                    </a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="/sms-batch/create">Create</a>
                        </li>
                        <li>
                            <a href="/sms-batch/index">Schedules</a>
                        </li>
                        <li>
                            <a href="/sms-logs/index">Reports</a>
                        </li>
                        <li>
                            <a href="/sms-batch/top-up">Top Up</a>
                        </li>
                    </ul>
                </li>
                <li class="level-1-menu">
                    <a href="javascript:;"><i class="sidebar-item-icon fa fa-wrench"></i>
                        <span class="nav-label">Settings</span><i class="fa fa-angle-left arrow"></i>
                    </a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="/casuals/index">Casuals</a>
                        </li>
                        <li>
                            <a href="/admins/index">Admins</a>
                        </li>
                        <li>
                            <a href="/email-templates/index">Notifications</a>
                        </li>
                        <li>
                            <a href="/mail-queue/index">Mail Queue</a>
                        </li>
                        <li>
                            <a href="/project-status/index">Project Status</a>
                        </li>
                        <li>
                            <a href="/measure-units/index">Measure Units</a>
                        </li>
                        <li>
                            <a href="/document-types/index">Document Types</a>
                        </li>
                        <li>
                            <a href="/system-settings/index">System Settings</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="sidebar-footer">
            </div>
        </div>
    </nav>