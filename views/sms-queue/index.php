<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SmsQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Queues';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-queue-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sms Queue', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'queue_id',
            'phone',
            'message:ntext',
            'status',
            'batch_id',
            //'customer_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
