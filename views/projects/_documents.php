<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$documentTypes = \app\models\DocumentTypes::find()->all();
$document = new \app\models\Documents();
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($documentTypes): foreach($documentTypes as $type):?>
        <label><?=$type->name?></label>
        <?=$form->field($document,'file')->fileInput(['name'=>'file_'.$type->document_type_id,'id'=>'document-file-'.$type->document_type_id])->label(false);?>
        <hr>
    <?php endforeach; endif;?>
   
    <div class="form-group">
        <?= $update ? NULL :  Html::a('Back', 'create', ['class' => 'btn btn-secondary btn-sm pull-left']) ?>
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <?= $update ? NULL : Html::a('Skip&nbsp;&nbsp;', ['create','step'=>3], ['class' => 'pull-right','style'=>'font-weight:bold;position:relative;top:3px;']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>

</div>
