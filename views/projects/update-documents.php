<?php
	use yii\helpers\Html;
	$this->title = 'Update Documents';
	$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $project->name, 'url' => ['view','id'=>$project->project_id]];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!--  -->
        </div>
    </div>

    <div class="ibox-body">
    	<?= $this->render('_documents', [
	        'update' => 1,
	    ]) ?>
    </div>

</div>

