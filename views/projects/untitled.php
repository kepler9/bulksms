<div class="col-md-6">
		<div class="form-group">
			<label>Item</label>
			<input class="form-control" name="item[]" data-validation="required">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			<label>Quantity</label>
			<input class="form-control" name="quantity[]" data-validation="number">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			<label>Unit</label>
			<input class="form-control" name="unit[]" data-validation="required">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group">
			<label>Rate</label>
			<input class="form-control" name="rate[]" data-validation="number" data-validation-allowing="float">
		</div>
	</div>