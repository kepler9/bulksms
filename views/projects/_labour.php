<?php
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin()?>
<div id="labour-items" class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>Title</label>
			<input class="form-control" name="title[]" data-validation="required">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Total Amount</label>
			<input class="form-control" name="total[]" data-validation="number">
		</div>
	</div>
</div>
<div class="form-group">
	<a id="add-labour-item" class="active" href="#"><i class="fa fa-plus-circle"></i> Add Labour Item</a>
</div>
<hr class="hr-25">
<div class="form-group">
    <?= Html::a('Back', ['projects/create','step'=>3],['class' => 'btn btn-secondary btn-sm pull-left']) ?>
    <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']); ?>
    <?= Html::a('Skip&nbsp;&nbsp;', ['view','id'=>$this->context->project->project_id], ['class' => 'pull-right','style'=>'font-weight:bold;position:relative;top:3px;']) ?>
    <span class="clearfix"></span>
</div>
<?php ActiveForm::end();?>
<script type="text/javascript">
	$('#add-labour-item').click(function(event) {
		event.preventDefault();
		$('#labour-items').append('<div class="col-md-6"><div class="form-group"><label>Title</label><input class="form-control" name="title[]" data-validation="required"></div></div><div class="col-md-6"><div class="form-group"><label>Total Amount</label><input class="form-control" name="total[]" data-validation="number"></div></div>');
	});
</script>