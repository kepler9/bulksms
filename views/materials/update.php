<?php
	use yii\helpers\Html;
	$type = $model->type ? 'Actual' : 'Estimated';
	$this->title = 'Update '.$type.' Materials: ' . $model->item;
	$this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
	$this->params['breadcrumbs'][] = ['label'=>$model->project->description,'url'=>['/projects/view','id'=>$model->project_id]];
	$this->params['breadcrumbs'][] = ['label' => $type.' Materials', 'url' => ['index','type'=>$model->type]];
	$this->params['breadcrumbs'][] = ['label' => $model->item, 'url' => ['view', 'id' => $model->materials_id]];
	$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="estimated-materials-update">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>