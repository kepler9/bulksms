<?php
    use yii\helpers\Html;
    $type = Yii::$app->request->get('type') ? 'Actual' : 'Estimated';
    $this->title = 'Add '.$type.' Materials';
    $this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
    $this->params['breadcrumbs'][] = ['label'=>$this->context->project->description,'url'=>['/projects/view','id'=>$this->context->project->project_id]];
    $this->params['breadcrumbs'][] = ['label' => $type.' Materials', 'url' => ['index','type'=>Yii::$app->request->get('type')]];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools"></div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>
