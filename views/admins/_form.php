<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Admins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admins-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notifications')->dropDownList([1=>'On',0=>'Off'],['prompt'=>'Select']) ?>   

    <?= $form->field($model, 'status')->dropDownList([1=>'Active',0=>'Inactive'],['prompt'=>'Select']) ?>    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>

    <?php ActiveForm::end(); ?>

</div>
