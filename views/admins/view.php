<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Admins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
           <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Yii::$app->user->identity->id != $model->id ? Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) : NULL; ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                'email:email',
                'phone_no',
                'date_created:date',
                'accountStatus:raw', 
                ['attribute' => 'notificationsStatus', 'format' => 'raw'],               
            ],
        ]) ?>
    </div>
</div>