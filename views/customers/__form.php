 <?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
 <?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'phone')->textInput(['maxlength' => true,'data-validation'=>'number','data-validation-optional'=>"true"]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true,'data-validation'=>'email','data-validation-optional'=>"true"]) ?>

<?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

<input type="hidden" name="form" value="create">

<label>Customer List | <a href="/customer-lists/create?redirect=<?=Yii::$app->request->url?>"><i class="fa fa-plus"></i> Add</a></label>
<?= $form->field($model,'list_id')->dropDownList(ArrayHelper::map($lists,'list_id','name'),['prompt'=>'Select'])->label(false);?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
    <span class="clearfix"></span>
</div>
<?php ActiveForm::end(); ?>