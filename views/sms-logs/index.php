<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Sms Reports';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td style="width: 50%;background-color: #f5f5f5;">Total Messages</td>
                    <td><b><?=number_format($dataProvider->totalCount);?></b></td>
                </tr>
                <tr>
                    <td style="width: 50%;background-color: #f5f5f5;">Total Cost</td>
                    <td><b>KSh <?=number_format(Yii::$app->session->getFlash('total_cost'),2);?></b></td>
                </tr>
            </tbody>
        </table>
        <div class="sms-logs-index table-responsive">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    ['attribute'=>'customer.name','label'=>'Customer'],
                    'message:ntext',
                    'phone',
                    ['attribute'=>'status','format'=>'raw','value'=>function($model){
                        return '<span class="badge badge-info">'.$model->status.'</span>';
                    }],
                    'cost',
                    'date_sent:date',

                    ['class' => 'yii\grid\ActionColumn','template'=>NULL],
                ],
            ]); ?>

        </div>
    </div>
</div>