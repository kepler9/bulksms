<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
\yii\web\YiiAsset::register($this);
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        ['attribute'=>'customer.name','label'=>'Customer'],
        'date_visited:date',
        'location:ntext',
        'distance',
        'fare:currency',
        'description:ntext',
        'area',
        'measurements:ntext',
        ['attribute'=>'sketch','value'=>function($model){
            return $this->render('//sites/_sketch',['model'=>$model]);
        },'format'=>'raw'],
        ['attribute'=>'photos','value'=>function($model){
            return $this->render('//sites/_photos',['model'=>$model]);
        },'format'=>'raw'],
        'notes:ntext',
        'date_created:date',
        ['attribute'=>'user.username','label'=>'Created By'],
    ],
]) ?>