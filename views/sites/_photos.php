<?php
use yii\helpers\Html;
echo newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox-photos-'.$model->site_id.']',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '100%',
        'height' => '100%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
?>
<?php if(isset($model->photos)): foreach(json_decode($model->photos) as $image):?>
    <?=Html::a(Html::img('/uploads/'.$image,['class'=>'photos-thumbs']), '/uploads/'.$image, ['rel' => 'fancybox-photos-'.$model->site_id]);?>
<?php endforeach; endif;?>
