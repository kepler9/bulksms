<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $this->title = 'Site Visits';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('<i class="fa fa-plus"></i> New Site Visit', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="sites-index table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    ['attribute'=>'customer.name','label'=>'Client'],
                    'date_visited:date',
                    'location:ntext',
                    'distance',
                    'fare:currency',
                    'area',
                    'measurements:ntext',
                    'date_created:date',
                    ['attribute'=>'project_id','label'=>'Project','value'=>function($model){
                        if($model->project_id){
                            return Html::a('<span style="color:#19C5A9"><b>Created</b></span>',['projects/view','id'=>$model->project_id]);
                        }else{
                            return Html::a('<b>Create</b>',['projects/create','site_id'=>$model->site_id]);
                        }
                    },'format'=>'raw'],
                    ['attribute'=>'user.username','label'=>'By'],

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>
        </div>
    </div>
</div>
