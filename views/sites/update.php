<?php
use yii\helpers\Html;
$this->title = 'Update: ' . $model->customer->name;
$this->params['breadcrumbs'][] = ['label' => 'Site Visits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->customer->name, 'url' => ['view', 'id' => $model->site_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title." - ".$model->description?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="sites-update">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>
