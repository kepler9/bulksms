<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use mihaildev\ckeditor\CKEditor;
?>

<div class="email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subject')->textarea(['rows' => 1]) ?>

    <?= $form->field( $model, 'body')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', 
            'inline' => false, 
        ],
        'options' => ['rows' => 4,'class'=>'form-control','id' => 'edit-template-'.$model->template_id],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right btn-sm']) ?>
        <span class="clearfix"></span>
    </div>

    <?php ActiveForm::end(); ?>

</div>
