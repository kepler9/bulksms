<?php

namespace app\controllers;

use Yii;
use app\models\Sites;
use app\models\SitesSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SitesController implements the CRUD actions for Sites model.
 */
class SitesController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SitesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sites model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($customer_id=null)
    {
        $model = new Sites();
        $model->user_id = Yii::$app->user->identity->id;
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $sketch = $this->model->upload_file(['field'=>'sketch']);
            if($sketch){
                $model->sketch = $sketch;
            }
            $photos = $this->model->upload_files(['field'=>'photos']);
            if($photos){
                $model->photos = json_encode($photos);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->site_id]);
            }
            $model->customer_id = $customer_id;
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $sketch = $this->model->upload_file(['field'=>'sketch']);
            if($sketch){
                $model->sketch = $sketch;
            }
            $photos = $this->model->upload_files(['field'=>'photos']);
            if($photos){
                $model->photos = json_encode($photos);
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->site_id]);
            }
        }
        
        $model->date_visited = date('Y-m-d',strtotime($model->date_visited));
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sites::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
