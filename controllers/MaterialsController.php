<?php

namespace app\controllers;

use Yii;
use app\models\Materials;
use app\models\MaterialsSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Projects;

/**
 * MaterialsController implements the CRUD actions for Materials model.
 */
class MaterialsController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Materials models.
     * @return mixed
     */
    public function actionIndex($type) {
        $searchModel = new MaterialsSearch();
        $searchModel->project_id = $this->project->project_id;
        $searchModel->type = $type;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Materials model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Materials model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type)
    {
        $model = new Materials();

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->total = $model->rate * $model->quantity;
            $model->project_id = $this->project->project_id;
            $model->type = $type;
            if ($model->save()) {
                return $this->redirect(['index', 'type' => $model->type]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Materials model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$redirect=null)
    {
        $model = $this->findModel($id);
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->total = $model->rate * $model->quantity;
            if ($model->save()) {
                Yii::$app->session->setFlash('msg','Updated');
                if($redirect){
                    return $this->redirect([$redirect]);
                }
                return $this->redirect(['index', 'type' => $model->type]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Materials model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $type = $model->type;
        $model->delete();
        return $this->redirect(['index','type'=>$type]);
    }

    /**
     * Finds the Materials model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Materials the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Materials::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
